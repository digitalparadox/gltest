package com.paradox.gorillabook.database.dao

import androidx.room.*
import com.paradox.gorillabook.domain.entity.Post
import io.reactivex.Completable
import io.reactivex.Single

@Dao
abstract class PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(post: Post) : Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(post: List<Post>) : Completable

    @Query("DELETE FROM Post")
    abstract fun deletePosts()

    @Query("SELECT * FROM Post ORDER BY unix_timestamp DESC")
    abstract fun posts() : Single<List<Post>>

}