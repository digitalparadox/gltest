package com.paradox.gorillabook.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.paradox.gorillabook.database.dao.PostDao
import com.paradox.gorillabook.domain.entity.Post

@Database(
    version = 1,
    entities = [
        Post::class
    ], exportSchema = false)
abstract class Database: RoomDatabase() {
    abstract fun postDao() : PostDao
}
