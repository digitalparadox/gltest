package com.paradox.gorillabook

import android.graphics.drawable.Drawable
import android.view.View
import android.view.View.*
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("bind:loadImageByUri", "placeholder", requireAll = false)
fun setGlideImageByUri(view: ImageView, url: String?, placeholderDrawable: Drawable?) {
    if (url != null) {
        GlideApp.with(view.context)
            .load(url)
            .apply {
                placeholderDrawable?.let { placeholder(it) }
            }
            .centerCrop()
            .into(view)
    }
}