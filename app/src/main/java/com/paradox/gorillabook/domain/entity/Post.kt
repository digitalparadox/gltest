package com.paradox.gorillabook.domain.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Post(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    var first_name: String,
    var last_name: String,
    var post_body: String,
    var unix_timestamp: Long?,
    var image: String?
)