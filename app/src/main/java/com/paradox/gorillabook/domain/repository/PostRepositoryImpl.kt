package com.paradox.gorillabook.domain.repository

import com.paradox.gorillabook.database.dao.PostDao
import com.paradox.gorillabook.domain.entity.Post
import com.paradox.gorillabook.network.Endpoint
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

class PostRepositoryImpl @Inject constructor(
    private val apiRepository: Endpoint,
    private val postDao: PostDao,
    @Named("ioScheduler") private val ioScheduler: Scheduler
) : PostRepository {

    override fun fetchPostsAndSave(): Completable {
        return apiRepository.posts()
            .subscribeOn(ioScheduler)
            .flatMapCompletable {
                postDao.deletePosts()
                postDao.insert(it)
            }
    }

    override fun fetchPosts(): Single<List<Post>> = postDao.posts().subscribeOn(ioScheduler)

    override fun createNewPost(post: Post): Completable = postDao.insert(post).subscribeOn(ioScheduler)

}