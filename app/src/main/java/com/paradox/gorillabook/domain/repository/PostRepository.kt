package com.paradox.gorillabook.domain.repository

import com.paradox.gorillabook.domain.entity.Post
import io.reactivex.Completable
import io.reactivex.Single

interface PostRepository {
    fun fetchPostsAndSave() : Completable
    fun fetchPosts() : Single<List<Post>>
    fun createNewPost(post: Post): Completable
}