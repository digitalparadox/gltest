package com.paradox.gorillabook.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.paradox.gorillabook.R
import com.paradox.gorillabook.databinding.FragmentSplashBinding
import dagger.android.support.AndroidSupportInjection

class SplashFragment : Fragment() {

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = DataBindingUtil.inflate<FragmentSplashBinding>(
        inflater,
        R.layout.fragment_splash,
        container,
        false
    ).root
}