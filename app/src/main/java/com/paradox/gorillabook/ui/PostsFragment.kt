package com.paradox.gorillabook.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatActivity.RESULT_OK
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.paradox.gorillabook.R
import com.paradox.gorillabook.BR
import com.paradox.gorillabook.databinding.FragmentPostsBinding
import com.paradox.gorillabook.domain.entity.Post
import com.paradox.gorillabook.ui.NewPostActivity.Companion.REQUEST_PICK_IMAGE
import com.paradox.gorillabook.ui.logic.viewmodel.PostViewModel
import com.paradox.gorillabook.ui.logic.viewmodel.ViewModelFactory
import com.paradox.gorillabook.ui.adapter.BoundRecyclerAdapter
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class PostsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[PostViewModel::class.java]
    }

    private lateinit var binding: FragmentPostsBinding
    private lateinit var boundAdapter: BoundRecyclerAdapter<Post>

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil
            .inflate<FragmentPostsBinding>(inflater, R.layout.fragment_posts, container, false)
            .also {
                it.lifecycleOwner = this@PostsFragment
                it.setVariable(BR.vm, viewModel)
            }

        boundAdapter = BoundRecyclerAdapter(viewModel) { R.layout.item_post }

        binding.recyclerPosts.apply {
            adapter = boundAdapter
            layoutManager = LinearLayoutManager(binding.recyclerPosts.context, VERTICAL, false)
        }

        observePosts()
        observeNavigations()

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == NEW_POST) {
            viewModel.onInit()
        }
    }

    private fun observePosts() {
        viewModel.posts.observe(viewLifecycleOwner, {
            boundAdapter.data = it
        })

        viewModel.onInit()
    }

    private fun observeNavigations() {
        viewModel.navigationEvents.observe(viewLifecycleOwner, {
            when (it) {
                is PostViewModel.PostsNavigation.OnCreateNewPost -> {
                    val intent = Intent(activity, NewPostActivity::class.java)
                    startActivityForResult(intent, NEW_POST)
                }
                else -> {}
            }
        })
    }

    companion object {
        const val NEW_POST = 200
    }
}