package com.paradox.gorillabook.ui.logic.viewmodel

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.paradox.gorillabook.domain.repository.PostRepository
import com.paradox.gorillabook.domain.entity.Post
import com.paradox.gorillabook.ui.logic.SingleLiveEvent
import com.paradox.gorillabook.ui.logic.viewmodel.PostViewModel.PostsNavigation.*
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import javax.inject.Inject
import javax.inject.Named

class PostViewModel @Inject constructor(
    private val postRepository: PostRepository,
    @Named("ioScheduler") private val ioScheduler: Scheduler
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val navigationEvents = SingleLiveEvent<PostsNavigation>()
    val posts = MutableLiveData<List<Post>>().apply { this.value = emptyList() }
    val newPost = MutableLiveData<Post>()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun onInit() {
        compositeDisposable.add(
            postRepository.fetchPosts()
                .observeOn(ioScheduler)
                .subscribe({
                    posts.postValue(it)
                }, {})
        )
    }

    fun preloadNewPost() {
        newPost.postValue(Post(0, "Esteban", "Lopez", "", Date().time, ""))
    }

    fun setImage(data: Uri?) {
        val post = newPost.value
        post?.apply {
            this.image = data.toString()
        }?.also {
            newPost.postValue(it)
        }
    }

    fun onSaveNewPost() {
        compositeDisposable.add(
            postRepository.createNewPost(newPost.value!!).observeOn(ioScheduler).subscribe({
                navigationEvents.postValue(OnCreateNewPostSuccess)
            }, {})
        )
    }

    fun onCreateNewPost() {
        navigationEvents.postValue(OnCreateNewPost)
    }

    fun onSelectPhotoFromGallery() {
        navigationEvents.postValue(OnSelectPhotoFromGallery)
    }


    sealed class PostsNavigation {
        object OnCreateNewPost : PostsNavigation()
        object OnCreateNewPostSuccess : PostsNavigation()
        object OnSelectPhotoFromGallery : PostsNavigation()
    }
}