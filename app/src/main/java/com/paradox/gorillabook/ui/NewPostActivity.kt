package com.paradox.gorillabook.ui

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.paradox.gorillabook.BR
import com.paradox.gorillabook.R
import com.paradox.gorillabook.databinding.NewPostActivityBinding
import com.paradox.gorillabook.ui.logic.viewmodel.PostViewModel
import com.paradox.gorillabook.ui.logic.viewmodel.PostViewModel.PostsNavigation.OnCreateNewPostSuccess
import com.paradox.gorillabook.ui.logic.viewmodel.PostViewModel.PostsNavigation.OnSelectPhotoFromGallery
import com.paradox.gorillabook.ui.logic.viewmodel.ViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class NewPostActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[PostViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<NewPostActivityBinding>(this, R.layout.new_post_activity).apply {
            lifecycleOwner = this@NewPostActivity
            setVariable(BR.vm, viewModel)
        }.also {
            setSupportActionBar(it.toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        viewModel.preloadNewPost()
        observeNavigationEvents()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_PICK_IMAGE) {
            viewModel.setImage(data?.data)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.creation_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_share -> {
            viewModel.onSaveNewPost()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED)
        finish()
    }

    private fun observeNavigationEvents() {
        viewModel.navigationEvents.observe(this, {
            when(it) {
                is OnSelectPhotoFromGallery -> {
                    val gallery = Intent(Intent.ACTION_PICK, INTERNAL_CONTENT_URI)
                    startActivityForResult(gallery, REQUEST_PICK_IMAGE)
                }
                is OnCreateNewPostSuccess -> {
                    setResult(RESULT_OK)
                    finish()
                }
                else -> {}
            }
        })
    }

    companion object {
        const val REQUEST_PICK_IMAGE = 101
    }
}