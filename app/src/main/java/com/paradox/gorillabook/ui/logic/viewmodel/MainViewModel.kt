package com.paradox.gorillabook.ui.logic.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import com.paradox.gorillabook.domain.repository.PostRepository
import com.paradox.gorillabook.ui.logic.SingleLiveEvent
import com.paradox.gorillabook.ui.logic.viewmodel.MainViewModel.MainNavigation.OnFetchPostsSuccess
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit.SECONDS
import javax.inject.Inject
import javax.inject.Named

class MainViewModel @Inject constructor(
    private val postRepository: PostRepository,
    @Named("ioScheduler") private val ioScheduler: Scheduler
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val navigationEvents = SingleLiveEvent<MainNavigation>()

    fun onInit() {
        compositeDisposable.add(
            postRepository.fetchPostsAndSave()
                .observeOn(ioScheduler)
                .delay(2, SECONDS)
                .subscribe({
                    navigationEvents.postValue(OnFetchPostsSuccess)
                }, {
                    Log.e("MainViewModel", it.message!!)
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    sealed class MainNavigation {
        object OnFetchPostsSuccess : MainNavigation()
    }
}