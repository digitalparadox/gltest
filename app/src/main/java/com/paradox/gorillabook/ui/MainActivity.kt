package com.paradox.gorillabook.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.paradox.gorillabook.R
import com.paradox.gorillabook.databinding.ActivityMainBinding
import com.paradox.gorillabook.ui.logic.viewmodel.MainViewModel
import com.paradox.gorillabook.ui.logic.viewmodel.ViewModelFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main).apply {
            lifecycleOwner = this@MainActivity
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_nav_host, SplashFragment())
                .commit()

        viewModel.navigationEvents.observe(this, {
            when (it) {
                MainViewModel.MainNavigation.OnFetchPostsSuccess -> {
                    supportFragmentManager.beginTransaction()
                            .replace(R.id.fragment_nav_host, PostsFragment())
                            .commit()
                }
            }
        })

        viewModel.onInit()
    }
}