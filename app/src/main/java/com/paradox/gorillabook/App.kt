package com.paradox.gorillabook

import android.app.Activity
import android.app.Application
import androidx.fragment.app.Fragment

import com.paradox.gorillabook.di.component.DaggerApplicationComponent
import com.paradox.gorillabook.di.module.AppModule
import com.paradox.gorillabook.di.module.NetworkModule
import com.paradox.gorillabook.di.module.RxModule
import com.paradox.gorillabook.di.component.ApplicationComponent
import com.paradox.gorillabook.network.Endpoint
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class App: Application(), HasActivityInjector, HasSupportFragmentInjector {

    private lateinit var component: ApplicationComponent
    private var apiRepository: Endpoint? = null

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var dispatchingFragmentInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent
            .builder()
            .appModule(AppModule(this))
            .networkModule(NetworkModule(apiRepository))
            .rxModule(RxModule())
            .build()

        component.injectMembers(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingActivityInjector

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = dispatchingFragmentInjector
}