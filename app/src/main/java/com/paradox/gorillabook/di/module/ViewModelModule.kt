package com.paradox.gorillabook.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.paradox.gorillabook.ui.logic.viewmodel.MainViewModel
import com.paradox.gorillabook.ui.logic.viewmodel.PostViewModel
import com.paradox.gorillabook.ui.logic.viewmodel.ViewModelFactory
import com.paradox.gorillabook.ui.logic.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModelModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostViewModel::class)
    abstract fun bindPostViewModel(postViewModel: PostViewModel): ViewModel
}
