package com.paradox.gorillabook.di.module

import com.paradox.gorillabook.domain.repository.PostRepository
import com.paradox.gorillabook.domain.repository.PostRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindAppRepository(postInteractor: PostRepositoryImpl) : PostRepository
}