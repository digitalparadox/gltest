package com.paradox.gorillabook.di.component

import com.paradox.gorillabook.di.module.ViewBindingModule
import com.paradox.gorillabook.App
import com.paradox.gorillabook.di.module.*
import com.paradox.gorillabook.di.module.AppModule
import com.paradox.gorillabook.di.module.DatabaseModule
import com.paradox.gorillabook.di.module.ViewModelModule
import dagger.Component
import dagger.MembersInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    DatabaseModule::class,
    NetworkModule::class,
    ViewBindingModule::class,
    ViewModelModule::class,
    RxModule::class,
    RepositoryModule::class
])
interface ApplicationComponent: MembersInjector<App>