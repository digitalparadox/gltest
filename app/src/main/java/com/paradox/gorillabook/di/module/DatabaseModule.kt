package com.paradox.gorillabook.di.module

import android.content.Context
import androidx.room.Room
import com.paradox.gorillabook.database.Database
import com.paradox.gorillabook.database.dao.PostDao
import dagger.Module
import dagger.Provides

@Module
internal class DatabaseModule(
    private var postDao: PostDao? = null
) {
    @Provides
    fun providesAuthDatabase(context: Context): Database =
        Room.databaseBuilder(context, Database::class.java, "db").build()

    @Provides
    fun providesPostDao(database: Database) = postDao ?: database.postDao().also { postDao = it }
}