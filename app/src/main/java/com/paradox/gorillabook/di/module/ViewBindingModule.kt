package com.paradox.gorillabook.di.module

import com.paradox.gorillabook.ui.MainActivity
import com.paradox.gorillabook.ui.NewPostActivity
import com.paradox.gorillabook.ui.PostsFragment
import com.paradox.gorillabook.ui.SplashFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ViewBindingModule {

    @ContributesAndroidInjector
    abstract fun providesMainActivity() : MainActivity

    @ContributesAndroidInjector
    abstract fun providesNewPostActivity() : NewPostActivity

    @ContributesAndroidInjector
    abstract fun providesPostsFragment() : PostsFragment

    @ContributesAndroidInjector
    abstract fun providesSplashFragment() : SplashFragment
}