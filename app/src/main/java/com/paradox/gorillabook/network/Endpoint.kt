package com.paradox.gorillabook.network

import com.paradox.gorillabook.domain.entity.Post
import io.reactivex.Single
import retrofit2.http.*

interface Endpoint {

    @Headers("Content-Type: application/json")
    @GET("/feed")
    fun posts(): Single<List<Post>>

}
